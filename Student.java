public class Student
{
	public int graduationYear;
	public String firstName;
	public boolean isGraduating;
	
	public void presentName()
	{
		System.out.println("Hi, I am " + this.firstName);
	}
	
	public void graduate()
	{
		if (this.isGraduating)
		{
			System.out.println("I am graduating this year!");
		}
		else
		{
			System.out.println("I am graduating in " + this.graduationYear);
		}			
	}
	
}

