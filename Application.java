public class Application
{
	public static void main (String[] args)
	{
		Student student1 = new Student();
		Student student2 = new Student();
		
		student1.graduationYear = 2023;
		student1.firstName = "XYZ";
		student1.isGraduating = student1.graduationYear == 2023;
		
		student2.graduationYear = 2025;
		student2.firstName = "ABC";
		student2.isGraduating = student2.graduationYear == 2023;
		
		System.out.println("Student " + student1.firstName + " is graduating this year.");
		System.out.println("That is " + student1.isGraduating + " as he/she is graduating in " + student1.graduationYear);
		
		System.out.println("Student " + student2.firstName + " is graduating this year.");
		System.out.println("That is " + student2.isGraduating + " as he/she is graduating in " + student2.graduationYear);
		
		student1.presentName();
		student1.graduate();
		
		student2.presentName();
		student2.graduate();
		
		Student[] section3 = new Student[3];
		section3[0] = student1;
		section3[1] = student2;
		
		section3[2] = new Student();
		section3[2].firstName = "MNO";
		section3[2].graduationYear = 2024;
		section3[2].isGraduating = section3[2].graduationYear == 2025;
		
		section3[2].presentName();
		section3[2].graduate();
	}
}